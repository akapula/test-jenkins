from helloworld import app
import unittest



class FlaskTestCase(unittest.TestCase):

	def test_index(self):
		tester = app.test_client(self)
		response = tester.get('/')
		self.assertEqual(response.status_code, 200)
		
	def test_index_error(self):
		tester = app.test_client(self)
		response = tester.get('/endgame')
		self.assertEqual(response.status_code, 404)	

	def test_index_number(self):
        	tester = app.test_client(self)
        	response = tester.get('/testNumber')
        	self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
	unittest.main()
